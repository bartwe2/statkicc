package domain;

public enum Direction {
    HORIZONTAL, VERTICAL
}
