package domain;

public enum State {
    EMPTY, HIT, MISSED, SUNK, SHIP
}
