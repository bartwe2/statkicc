package domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
@ToString
public class Coordinate {
    private int x;
    private int y;
}
