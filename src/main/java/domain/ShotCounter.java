package domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShotCounter {

    private int countOfMissedShots;

    private int countOfHitShots;

    public ShotCounter() {
        this.countOfMissedShots = 0;
        this.countOfHitShots = 0;
    }

    public void addMissedShot() {
        countOfMissedShots++;
    }

    public void addHitShot() {
        countOfHitShots++;
    }
}
