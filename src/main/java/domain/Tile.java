package domain;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class Tile {

    private State state = State.EMPTY;

    public Tile(final State state) {
        this.state = state;
    }
}
