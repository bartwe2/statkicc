package domain;

import domain.exception.ShipCollision;
import lombok.Getter;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Getter
public class Board {

    private Tile[][] tiles;

    private Set<Ship> placedShips = new HashSet<>();

    private int numberOfActiveShips = 0;

    private int sizeX;

    private int sizeY;

    public Board(final int sizeX, final int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        initializeBoard();
    }

    public void putShip(final Ship ship) throws ShipCollision {
        for (Coordinate newCoordinate : ship.getCoordinatesWithStates().keySet()) {
            for (Ship placedShip : placedShips) {
                if (placedShip.getCoordinatesWithStates().containsKey(newCoordinate)) {
                    throw new ShipCollision(newCoordinate);
                }
            }
        }
        this.placedShips.add(ship);
        numberOfActiveShips++;
    }

    public State shot(final Coordinate coordinate) {
        this.tiles[coordinate.getX()][coordinate.getY()] = new Tile(checkShot(coordinate));
        return checkShot(coordinate);
    }

    public State checkShot(final Coordinate coordinate) {
        //TODO DO POPRAWY STRZELANIE
        for (Ship ship : placedShips) {
            for (Map.Entry<Coordinate, State> entry : ship.getCoordinatesWithStates().entrySet()) {
                if (coordinate.getX() == entry.getKey().getX() && coordinate.getY() == entry.getKey().getY()) {
                    ship.setLifes(ship.getLifes() - 1);
                    if (ship.getLifes() == 0) {
                        numberOfActiveShips--;
                        changeShipStatusToSunk(ship);
                        return State.SUNK;
                    }
                    return State.HIT;
                }
            }
        }
        return State.MISSED;
    }

    public void changeShipStatusToSunk(Ship ship) {
        for (Map.Entry<Coordinate, State> entry : ship.getCoordinatesWithStates().entrySet()) {
            entry.setValue(State.SUNK);
            this.tiles[entry.getKey().getX()][entry.getKey().getY()] = new Tile(State.SUNK);
        }
    }

    private void initializeBoard() {
        //TODO TILE BEZ SENSU
        this.tiles = new Tile[this.sizeX][this.sizeY];
        for (int i = 0; i < this.sizeX; i++) {
            for (int j = 0; j < this.sizeY; j++) {
                this.tiles[i][j] = new Tile();
            }
        }
    }
}