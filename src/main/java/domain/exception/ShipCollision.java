package domain.exception;

import domain.Coordinate;

public class ShipCollision extends Exception {

    public ShipCollision(Coordinate coordinate) {
        super("Cant put ship on: " + coordinate);
    }
}