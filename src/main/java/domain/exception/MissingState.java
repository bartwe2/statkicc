package domain.exception;

public class MissingState extends RuntimeException {

    public MissingState() {
        super("Missing state");
    }
}
