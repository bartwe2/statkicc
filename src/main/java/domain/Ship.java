package domain;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class Ship {

    private Map<Coordinate, State> coordinatesWithStates = new HashMap<>();

    @Setter
    private int lifes;

    public Ship(final Coordinate coordinate, final Direction direction, final Size size) {
        for (int i = 0; i < size.getValue(); i++) {
            if (direction.equals(Direction.HORIZONTAL)) {
                coordinatesWithStates.put(Coordinate.of(coordinate.getX() + i, coordinate.getY()), State.SHIP);
            } else {
                coordinatesWithStates.put(Coordinate.of(coordinate.getX(), coordinate.getY() + i), State.SHIP);
            }
        }
        lifes = size.getValue();
    }
}
