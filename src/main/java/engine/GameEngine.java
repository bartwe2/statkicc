package engine;

import domain.Board;
import domain.Coordinate;
import domain.ShotCounter;
import domain.State;
import view.BoardView;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class GameEngine {

    private Board board;

    private Set<Coordinate> shotHistory = new HashSet<>();

    private ShotCounter shotCounter = new ShotCounter();

    public GameEngine(Board board) {
        this.board = board;
    }

    public void start() {
        System.out.println("Legend ---- EMPTY: 'O'; HIT: 'X'; MISSED: '*'; SUNK: '-'");

        mainLoop();

        System.out.println("End game.");
        System.out.println("Number of hits: " + shotCounter.getCountOfHitShots());
        System.out.println("Number of missed shots: " + shotCounter.getCountOfMissedShots());
    }

    private void mainLoop() {
        BoardView boardView = new BoardView(board);
        Drawer.print(boardView);

        while (board.getNumberOfActiveShips() != 0) {
            String shot = readShotFromInput();
            while (!canParseInput(shot)) {
                shot = readShotFromInput();
            }

            Coordinate shootCoordinate = parseStringToCoordinate(shot);
            if (!shotHistory.contains(shootCoordinate)) {
                State stateForShotCounter = board.shot(shootCoordinate);
                if (stateForShotCounter == State.HIT || stateForShotCounter == State.SUNK)
                    shotCounter.addHitShot();
                if (stateForShotCounter == State.MISSED)
                    shotCounter.addMissedShot();
                shotHistory.add(shootCoordinate);
                Drawer.print(boardView);
            } else {
                System.out.println("This field is already after shot");
            }
        }
    }

    protected boolean canParseInput(String shot) {
        try {
            Coordinate coordinate = parseStringToCoordinate(shot);
            return coordinate.getX() < board.getSizeX() && coordinate.getY() < board.getSizeY() &&
                    coordinate.getX() >= 0 && coordinate.getY() >= 0;
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
            return false;
        }
    }

    protected Coordinate parseStringToCoordinate(String string) {
        return Coordinate.of(Integer.parseInt(string.split(" ")[0]),
                Integer.parseInt(string.split(" ")[1]));
    }

    private String readShotFromInput() {
        System.out.println("\nEnter shot coordinates (separated by space): ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
