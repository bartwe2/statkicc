package engine;

import domain.Board;
import domain.randomizer.ShipCount;
import domain.randomizer.ShipType;

import java.util.HashMap;
import java.util.Map;

public class StartGame {

    private static Map<ShipType, ShipCount> shipTypeToShipCount = new HashMap<>();

    private static int boardSizeX = 5;

    private static int boardSizeY = 5;

    //deadline 31.12
    public static void main(String[] args) {
        shipTypeToShipCount.put(ShipType.of(1), ShipCount.of(2));
        shipTypeToShipCount.put(ShipType.of(2), ShipCount.of(2));
        shipTypeToShipCount.put(ShipType.of(3), ShipCount.of(1));
        shipTypeToShipCount.put(ShipType.of(4), ShipCount.of(1));
        shipTypeToShipCount.put(ShipType.of(5), ShipCount.of(1));

        Board board = new Board(boardSizeX, boardSizeY);

        new Randomizer().putShipsOnBoard(board, shipTypeToShipCount);
        new GameEngine(board).start();
    }
}
