package engine;

import domain.Tile;
import domain.exception.MissingState;
import view.BoardView;

public class Drawer {

    private Drawer() {
    }

    public static void print(final BoardView board) {
        for (int i = 0; i < board.getSizeY(); i++) {
            for (int j = 0; j < board.getSizeX(); j++) {
                System.out.print(resolveState(board.getTiles()[j][i]) + "  ");
            }
            System.out.println();
        }
    }

    private static char resolveState(final Tile tile) {
        switch (tile.getState()) {
            case EMPTY:
                return 'O';
            case HIT:
                return 'X';
            case MISSED:
                return '*';
            case SUNK:
                return '-';
            case SHIP:
                return 'A';
            default:
                throw new MissingState();
        }
    }
}
