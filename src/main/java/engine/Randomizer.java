package engine;

import domain.*;
import domain.exception.ShipCollision;
import domain.randomizer.ShipCount;
import domain.randomizer.ShipType;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Randomizer {

    /**
     * That method will try to put ship on board, if ship cannot be placed (after 100 times),
     * the ship will be skipped.
     */
    public void putShipsOnBoard(Board board, Map<ShipType, ShipCount> paramsToGenerate) {
        List<ShipType> flatParamsToGenerate = new ArrayList<>();
        paramsToGenerate.forEach((shipType, shipCount) -> {
            for (int i = 0; i < shipCount.getValue(); i++) {
                flatParamsToGenerate.add(shipType);
            }
        });

        flatParamsToGenerate.forEach(shipType ->
                {
                    int maxRetryTimes = 100;
                    while (maxRetryTimes > 0) {
                        try {
                            board.putShip(generateRandomShipByType(shipType, board));
                            break;
                        } catch (ShipCollision shipCollision) {
                            maxRetryTimes--;
                        }
                    }
                }
        );
    }

    private Ship generateRandomShipByType(ShipType shipType, Board board) {
        Direction direction = randomDirection();
        Coordinate coordinate;
        if (direction.equals(Direction.HORIZONTAL)) {
            coordinate = Coordinate.of(new SecureRandom().nextInt(board.getSizeX() - shipType.getValue() + 1),
                    new SecureRandom().nextInt(board.getSizeY()));
        } else {
            coordinate = Coordinate.of(new SecureRandom().nextInt(board.getSizeX()),
                    new SecureRandom().nextInt(board.getSizeY() - shipType.getValue() + 1));
        }
        return new Ship(coordinate, direction, Size.of(shipType.getValue()));
    }

    private Direction randomDirection() {
        return Direction.values()[new SecureRandom().nextInt(Direction.values().length)];
    }

}