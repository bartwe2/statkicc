package view;

import domain.Board;
import domain.State;
import domain.Tile;
import lombok.Getter;

@Getter
public class BoardView {

    private Tile[][] tiles;

    private int sizeX;

    private int sizeY;

    public BoardView(final Board board) {
        this.tiles = board.getTiles();
        this.sizeX = board.getSizeX();
        this.sizeY = board.getSizeY();
        this.putShipsOnTiles(board);
    }

    private void putShipsOnTiles(final Board board) {
        board.getPlacedShips().forEach(ship -> ship.getCoordinatesWithStates()
                .forEach((key, value) -> tiles[key.getX()][key.getY()] = new Tile(hideShipStateForPlayer(value))));
    }

    private State hideShipStateForPlayer(State value) {
        if (value.equals(State.SHIP)) {
            return State.EMPTY;
        }
        return value;
    }
}