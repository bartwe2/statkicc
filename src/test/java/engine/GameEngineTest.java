package engine;

import domain.Board;
import domain.Coordinate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameEngineTest {

    public static final int SIZE_X = 10;
    public static final int SIZE_Y = 10;

    public static final String SHOT_INPUT_CORRECT = "1 1";
    public static final String SHOT_INPUT_WRONG = "22";
    public static final String SHOT_INPUT_WRONG_CHAR = "AA";
    public static final String SHOT_INPUT_OUT_OF_RANGE = "" + SIZE_X + " " + SIZE_Y;

    private GameEngine gameEngine;

    @BeforeEach()
    void setUp() {
        gameEngine = new GameEngine(new Board(SIZE_X, SIZE_Y));
    }

    @Test
    void shouldParseInput() {
        assertTrue(gameEngine.canParseInput(SHOT_INPUT_CORRECT));
    }

    @Test
    void shouldFailToParseWrongInput() {
        assertFalse(gameEngine.canParseInput(SHOT_INPUT_WRONG));
        assertFalse(gameEngine.canParseInput(SHOT_INPUT_WRONG_CHAR));
    }

    @Test
    void shouldFailToParseInputOutOfRange() {
        assertFalse(gameEngine.canParseInput(SHOT_INPUT_OUT_OF_RANGE));
    }

    @Test
    void shouldParseStringToCoordinate() {
        Coordinate coordinate = Coordinate.of(2,5);
        Coordinate parsedCoordinate = gameEngine.parseStringToCoordinate("2 5");
        assertEquals(coordinate, parsedCoordinate);
    }


}