package domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ShotCounterTest {

    private ShotCounter shotCounter;

    @BeforeEach
    void setup() {
        this.shotCounter = new ShotCounter();
    }

    @Test
    void shouldAddHitShotToCounter() {
        shotCounter.addHitShot();
        shotCounter.addHitShot();
        shotCounter.addHitShot();
        assertEquals(3, shotCounter.getCountOfHitShots());
    }

    @Test
    void shouldAddMissedShotToCounter() {
        shotCounter.addMissedShot();
        shotCounter.addMissedShot();
        assertEquals(2, shotCounter.getCountOfMissedShots());
    }
}