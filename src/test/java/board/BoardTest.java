package board;

import domain.*;
import domain.exception.ShipCollision;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BoardTest {

    private Board board;

    public static final int SIZE_X = 10;
    public static final int SIZE_Y = 10;

    @BeforeEach()
    void setUp() {
        board = new Board(SIZE_X, SIZE_Y);
    }

    @Test
    void shouldThrowExceptionWhenPutShipInThisSamePlace() {
        Assertions.assertThrows(ShipCollision.class, () -> {
            board.putShip(new Ship(Coordinate.of(5, 5), Direction.HORIZONTAL, Size.of(3)));
            board.putShip(new Ship(Coordinate.of(5, 5), Direction.HORIZONTAL, Size.of(3)));
        });
    }

    @Test
    void shouldPutShip() throws ShipCollision {
        board.putShip(new Ship(Coordinate.of(0, 9), Direction.HORIZONTAL, Size.of(3)));
        assertEquals(1, board.getPlacedShips().size());
    }

    @Test
    void shouldChangeShipStatusToSunk() {
        Ship ship = new Ship(Coordinate.of(0, 0), Direction.HORIZONTAL, Size.of(1));
        board.changeShipStatusToSunk(ship);
        assertEquals(State.SUNK, board.getTiles()[0][0].getState());
    }

    @Test
    void shouldMissShot() {
        board.shot(Coordinate.of(0, 0));
        assertEquals(State.MISSED, board.getTiles()[0][0].getState());
    }

    @Test
    void shouldHitShot() throws ShipCollision {
        board.putShip(new Ship(Coordinate.of(0, 0), Direction.HORIZONTAL, Size.of(3)));
        board.shot(Coordinate.of(2, 0));
        assertEquals(State.HIT, board.getTiles()[2][0].getState());
    }


}